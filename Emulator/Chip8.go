package Emulator

import (
    "GoChip/GFX"
    "github.com/veandco/go-sdl2/sdl"
)

var Window GFX.Window

func Init() {
    Window = GFX.NewWindow("Chip8", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED, 64, 32, true)
    
    DoLoop(false, 60, Window)
}