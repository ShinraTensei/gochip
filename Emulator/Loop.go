package Emulator

import (
    "GoChip/GFX"
    "github.com/veandco/go-sdl2/sdl"
    "fmt"
    "math/rand"
)

func DoLoop(limitFPS bool, fpsLimit int, ws GFX.Window) {
    
    //Load rom
    LoadRom("res/Zero.ch8")
    
        
    for ws.IsRunning {
        decodeOpcodes(ws)
        
        for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
            switch event.(type) {
                case *sdl.QuitEvent:
                    ws.IsRunning = false
            }
        }
        err := ws.Renderer.Clear()
        if err != nil {
            panic(err)
        }
        
        //render shit here
        GFX.DrawRects(ws)
        
        
        ws.Renderer.Present()
        //sdl.Delay(16)
    }
    sdl.Quit()
}

func decodeOpcodes(ws GFX.Window) {
    //Fetch opcode
    Sp.opcode = uint16(Sp.memory[Sp.pc]) << 8 | uint16(Sp.memory[Sp.pc+1])
    
    //Decode opcode
    switch Sp.opcode & 0xF000 {
        case 0x0000:
            switch Sp.opcode & 0x00FF {
                case 0x00E0: //00E0: Clears the screen.
                    for i := 0; i < len(GFX.RectState); i++ {
                        GFX.RectState[i] = 0
                    }
                    Sp.pc +=2
                case 0x00EE:
                    Sp.pc = uint16(Sp.stack[Sp.stack_pointer])
                    Sp.stack_pointer--
                default:
                    fmt.Printf("Unknown opcode 0x%x\n", Sp.opcode)
            }
        case 0xA000: //ANNN: Sets I to the address NNN.
            Sp.I = Sp.opcode & 0x0FFF
            Sp.pc += 2
        /*case 0xB000: //BNNN: Jumps to the address NNN plus V0.
            Sp.pc = (Sp.opcode & 0x0FFF)*/
        case 0xC000: //CXNN: Sets VX to a random number, masked by NN.
            Sp.V[(Sp.opcode & 0x0F00) >> 8] = uint8(uint16(rand.Int()) & (Sp.opcode & 0x0FF))
            Sp.pc += 2
        case 0xD000:
            /*Sprites stored in memory at location in index register (I),
            maximum 8bits wide. Wraps around the screen. If when drawn, clears a pixel,
            register VF is set to 1 otherwise it is zero. All drawing is XOR drawing
            (i.e. it toggles the screen pixels)*/
            x := uint16(Sp.V[(Sp.opcode & 0x0F00) >> 8])
            y := uint16(Sp.V[(Sp.opcode & 0x00F0) >> 4])
            h := (Sp.opcode & 0x000F)
            Sp.V[0xF] = 0
            
            for yy := uint16(0); yy < h && yy + y < uint16(ws.H); yy++ {
                var px uint8 = Sp.memory[Sp.I+uint16(yy)]
                
                for xx := uint16(0); xx < 8; xx++ {
                    if (px & (0x80 >> xx)) != 0  {
	                    offset := (x + xx + ((y + yy) * uint16(ws.W)))
                        if GFX.RectState[offset] == 1 {
                            Sp.V[0xF] = 1
                        }
                        GFX.RectState[offset] ^= 1
                        //GFX.EnableState(int(offset))
                    }
                }
            }
            Sp.pc += 2
        case 0xE000:
            switch Sp.opcode & 0x00FF {
                default:
                    fmt.Printf("Unknown opcode 0x%x\n", Sp.opcode)
            }
        
        case 0x1000: //1NNN: Jumps to address NNN.
            Sp.pc = (Sp.opcode & 0x0FFF)
        case 0x2000: //2NNN: Calls subroutine at NNN
            Sp.stack_pointer++
            Sp.stack[Sp.stack_pointer] = uint8(Sp.pc)
            Sp.pc = Sp.opcode & 0x0FFF
        case 0x3000: //3XNN: Skips the next instruction if VX equals NN.
            if Sp.V[(Sp.opcode & 0x0F00) >> 8] == uint8(Sp.opcode & 0x00FF) {
                Sp.pc += 4
            }else{
                Sp.pc += 2
            }
        case 0x4000: //4XNN: Skips the next instruction if VX doesn't equal NN.
            if Sp.V[(Sp.opcode & 0x0F00) >> 8] != uint8(Sp.opcode & 0x00FF) {
                Sp.pc += 4
            }else{
                Sp.pc += 2
            }
        case 0x5000: //5XY0 Skips the next instruction if VX equals VY.
            if Sp.V[(Sp.opcode & 0x0F00) >> 8] == Sp.V[(Sp.opcode & 0x00F0) >> 4] {
                Sp.pc += 4
            }
            Sp.pc += 2
        case 0x6000: //6XNN: Sets VX to NN
            Sp.V[(Sp.opcode & 0x0F00) >> 8] = uint8(Sp.opcode & 0x00FF)
            Sp.pc += 2
        case 0x7000: //7XNN: Adds NN to VX
            Sp.V[(Sp.opcode & 0x0F00) >> 8] += uint8(Sp.opcode & 0x00FF)
            Sp.pc += 2
            
        case 0x8000: //8XYN
            switch Sp.opcode & 0x000F {
                case 0x0000: //0000: Sets VX to the value of VY.
                    Sp.V[(Sp.opcode & 0x0F00) >> 8] = Sp.V[(Sp.opcode & 0x00F0) >> 4]
                    Sp.pc += 2
                case 0x0004: //8XY4: Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't.
                    add := Sp.V[(Sp.opcode & 0x0F00) >> 8] + Sp.V[(Sp.opcode & 0x00F0) >> 4]
                    Sp.V[(Sp.opcode & 0x0F00) >> 8] = add & 0x00FF
                    Sp.V[0xF] = (add >> 8) & 0x1
                    Sp.pc += 2
                case 0x0005: //8XY5: VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
                    sub := Sp.V[(Sp.opcode & 0x0F00) >> 8] - Sp.V[(Sp.opcode & 0x00F0) >> 4]
                    if sub < 0 {
                        sub = 0
                        Sp.V[0xF] = 0
                    }else{
                        Sp.V[0xF] = 1
                    }
                    Sp.V[(Sp.opcode & 0x0F00) >> 8] = sub
                    Sp.pc += 2
                default:
                    fmt.Printf("Unknown opcode 0x%x\n", Sp.opcode)
            }
        case 0x9000: //9XY0: Skips the next instruction if VX doesn't equal VY.
            if Sp.V[(Sp.opcode & 0x0F00) >> 8] != Sp.V[(Sp.opcode & 0x00F0) >> 4] {
                Sp.pc += 4
            }else{
                Sp.pc += 2
            }
        case 0xF000:
            switch Sp.opcode & 0x00FF {
                case 0x0007: //FX07: Sets VX to the value of the delay timer.
                    Sp.V[(Sp.opcode & 0x0F00) >> 8] = Sp.delay_timer
                    Sp.pc += 2
                case 0x0015: //FX15: Sets the delay timer to VX.
                    Sp.delay_timer = Sp.V[(Sp.opcode & 0x0F00) >> 8]
                    Sp.pc += 2
                case 0x001E: //FX1E: Adds VX to I
                    Sp.I += uint16(Sp.V[(Sp.opcode & 0x0F00) >> 8])
                    Sp.pc += 2
                case 0x0033:
                    /*FX33: Stores the Binary-coded decimal representation of VX,
                    *with the most significant of three digits at the address in I,
                    *the middle digit at I plus 1, and the least significant digit at I plus 2.
                    *(In other words, take the decimal representation of VX, place the hundreds digit in
                    *memory at location in I, the tens digit at location I+1,
                    *and the ones digit at location I+2.)
                    */
                    Sp.memory[Sp.I] = uint8(Sp.V[(Sp.opcode & 0x0F00) >> 8] / 100 % 10)
                    Sp.memory[Sp.I+1] = uint8(Sp.V[(Sp.opcode & 0x0F00) >> 8] / 10 % 10)
                    Sp.memory[Sp.I+2] = uint8(Sp.V[(Sp.opcode & 0x0F00) >> 8] % 10)
                    
                case 0x0055: //FX55: Stores V0 to VX in memory starting at address I.
                    for i := uint16(0); i < ((Sp.opcode & 0x0F00) >> 8); i++ {
                        Sp.memory[Sp.I+i] = Sp.V[i]
                    }
                    Sp.pc += 2
                case 0x0065: //FX65: Fills V0 to VX with values from memory starting at address I
                    for i := uint16(0); i < ((Sp.opcode & 0x0F00) >> 8); i++ {
                        Sp.V[i] = Sp.memory[Sp.I+i]
                    }
                    Sp.pc += 2
                default:
                    fmt.Printf("Unknown opcode 0x%x\n", Sp.opcode)
            }
        default:
            fmt.Printf("Unknown opcode 0x%x\n", Sp.opcode)
    }
    
    //Update timers
    if Sp.delay_timer > 0 {
        Sp.delay_timer--
    }
    if Sp.sound_timer > 0 {
        if Sp.sound_timer == 1 {
            fmt.Println("BEEP")
            Sp.sound_timer--
        }
    }
}