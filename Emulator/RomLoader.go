package Emulator

import (
    "fmt"
    "io/ioutil"
)

func LoadRom(filePath string) {
    //load file
    buffer, err := ioutil.ReadFile(filePath)
    if err != nil {
        fmt.Println("Failed to load ", filePath)
        panic(err)
    }else {
        fmt.Println("Loaded ", filePath)
        fmt.Println(filePath, "is " , len(buffer) , " bytes")
    }
    
    //load into memory
    for i := 0; i < len(buffer); i++ {
        Sp.memory[i+0x200] = buffer[i] 
    }
}