package GFX

import (
    "github.com/veandco/go-sdl2/sdl"
)

var RectState [64*32]int32
var Rect sdl.Rect

func init() {
    for i := 0; i < len(RectState); i++ {
        RectState[i] = 0
    }
    
    Rect = sdl.Rect{0, 0, ScaleFactor, ScaleFactor}
}

func DrawRects(ws Window) {
    var y int32
    var x int32
    var id int32 = 0
    
    for y = 0; y < int32(ws.H); y++ {
        for x = 0; x < int32(ws.W); x++ {
            if id > int32(ws.W*ws.H) {
                id = 0
            }
            if RectState[id] == 1 {
                Rect.X = x*ScaleFactor
                Rect.Y = y*ScaleFactor
                ws.Renderer.SetDrawColor(255, 255, 255, 255)
                ws.Renderer.FillRect(&Rect)
                ws.Renderer.SetDrawColor(255, 0, 0, 255)
                ws.Renderer.DrawRect(&Rect)
                
                //back to default
                ws.Renderer.SetDrawColor(0, 0, 0, 255)
            }
            id++
        }
    }
}

func EnableState(i int) {
    RectState[i] = 1
}
func DisableState(i int) {
    RectState[i] = 0
}