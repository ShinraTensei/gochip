package GFX

import (
    "github.com/veandco/go-sdl2/sdl"
)

type Window struct {
    window *sdl.Window
    Renderer *sdl.Renderer
    x int
    y int
    W int
    H int
    title string
    IsRunning bool
}
var ScaleFactor int32 = 15

func init() {
    err := sdl.Init(sdl.INIT_EVERYTHING)
    if err != nil {
        panic(err)
    }
}

func NewWindow(title string, x int, y int, w int, h int, isRunning bool) Window {
    ws := Window{nil, nil, x, y, w, h, title, isRunning}
    
    var err error
    ws.window, err = sdl.CreateWindow(ws.title, ws.x, ws.y, ws.W*int(ScaleFactor), ws.H*int(ScaleFactor), sdl.WINDOW_SHOWN)
    if err != nil {
        panic(err)
    }
    
    ws.Renderer, err = sdl.CreateRenderer(ws.window, -1, sdl.RENDERER_ACCELERATED)
    if err != nil {
        panic(err)
    }
    
    return ws
}